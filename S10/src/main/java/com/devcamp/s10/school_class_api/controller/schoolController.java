package com.devcamp.s10.school_class_api.controller;

import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.school_class_api.model.school;
import com.devcamp.s10.school_class_api.service.schoolService;

@RestController
@RequestMapping
@CrossOrigin

public class schoolController {
    
    @Autowired
    schoolService schoolService;

    @GetMapping("/schools")
    public ArrayList<school> getAllSchoolApi(){
        return schoolService.getAllSchools();
    }
    
    @GetMapping("/school-by-id")
    public school getSchoolByIdApi(@RequestParam(name="id", required = true) int id){
        return schoolService.getSchoolById(id);
    }

    @GetMapping("/school-by-number")
    public ArrayList<school> getSchoolByNumberApi(@RequestParam(name="noNumber", required = true) int noNumber){
        return schoolService.getSchoolByInputNoStudent(noNumber);
    }

}
