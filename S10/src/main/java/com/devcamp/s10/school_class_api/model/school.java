package com.devcamp.s10.school_class_api.model;

import java.util.ArrayList;

public class school {
    private int id;
    private String name;
    private String address;
    private ArrayList<classroome> classrooms;
    
    public school() {
    }

    public school(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public school(int id, String name, String address, ArrayList<classroome> classrooms) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.classrooms = classrooms;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<classroome> getClassrooms() {
        return classrooms;
    }

    public void setClassrooms(ArrayList<classroome> classrooms) {
        this.classrooms = classrooms;
    }

    public int getTotalStudent(){
        int totalStudent = 0;
        for (classroome classes: classrooms){
            totalStudent += classes.getNoStudent();
        }
        return totalStudent;
    }
    
}
