package com.devcamp.s10.school_class_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolClassApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolClassApiApplication.class, args);
	}

}
