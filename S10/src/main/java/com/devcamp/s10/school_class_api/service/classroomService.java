package com.devcamp.s10.school_class_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s10.school_class_api.model.classroome;

@Service
public class classroomService {
    
    classroome grade1 = new classroome(101, "Lop 1", 15);
    classroome grade2 = new classroome(102, "Lop 2", 12);
    classroome grade3 = new classroome(103, "Lop 3", 13);

    classroome grade7 = new classroome(107, "Lop 7", 25);
    classroome grade8 = new classroome(108,"Lop 8", 25);
    classroome grade9 = new classroome(109, "Lop 9", 15);

    classroome grade10 = new classroome(110, "Lop 10", 25);
    classroome grade11 = new classroome(111, "Lop 11", 20);
    classroome grade12 = new classroome(112, "Lop 12", 30);


    public ArrayList<classroome> getElementarySchool(){
        ArrayList<classroome> elementary = new ArrayList<>();

        elementary.add(grade1);
        elementary.add(grade2);
        elementary.add(grade3);

        return elementary;
    }

    public ArrayList<classroome> getSecondarySchool(){
        ArrayList<classroome> secondary = new ArrayList<>();

        secondary.add(grade7);
        secondary.add(grade8);
        secondary.add(grade9);

        return secondary;
    }

    public ArrayList<classroome> getHightSchool(){
        ArrayList<classroome> hightSchool = new ArrayList<>();

        hightSchool.add(grade10);
        hightSchool.add(grade11);
        hightSchool.add(grade12);

        return hightSchool;
    }

    public ArrayList<classroome> getAllClasses(){
        ArrayList<classroome> allClasses = new ArrayList<>();
        allClasses.add(grade1);
        allClasses.add(grade2);
        allClasses.add(grade3);
        allClasses.add(grade7);
        allClasses.add(grade8);
        allClasses.add(grade9);
        allClasses.add(grade10);
        allClasses.add(grade11);
        allClasses.add(grade12);
        
        return allClasses;
    }

    public ArrayList<classroome> getClassByInputNoStudent(int inpNumber){
        ArrayList<classroome> target = new ArrayList<>();
        for (int i=0; i<getAllClasses().size(); i++){
            if (getAllClasses().get(i).getNoStudent() > inpNumber){
                target.add(getAllClasses().get(i));
            }
        }
        return target;
    }

}
