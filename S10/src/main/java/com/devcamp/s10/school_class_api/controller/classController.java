package com.devcamp.s10.school_class_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.school_class_api.model.classroome;
import com.devcamp.s10.school_class_api.service.classroomService;

@RestController
@RequestMapping("/")
@CrossOrigin

public class classController {
    @Autowired
    classroomService classroomService;

    @GetMapping("/classess")
    public ArrayList<classroome> getAllClassesApi(){
        return classroomService.getAllClasses();
    }

    @GetMapping("/numberstudent")
    public ArrayList<classroome> getClassByInputNoStudentApi(@RequestParam(name = "noNumber", required = true) int inpNumber){
        return classroomService.getClassByInputNoStudent(inpNumber);
    }

}
