package com.devcamp.s10.school_class_api.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s10.school_class_api.model.school;

@Service
public class schoolService {
    @Autowired
    public classroomService classroom  = new classroomService()     ;
    
    school vinSchool = new school(123, "Cap 1", "37 Ton That Dam", classroom.getElementarySchool());
    school usaSchool = new school(789, "Cap 2", "69 Bui Thi Xuan", classroom.getSecondarySchool());
    school canSchool = new school(101112, "Cap 3", "39 Song Hanh", classroom.getHightSchool());

//get list of schools
public ArrayList<school> getAllSchools(){
    ArrayList<school> allSchools = new ArrayList<>();
    allSchools.add(vinSchool);
    allSchools.add(usaSchool);
    allSchools.add(canSchool);

    return allSchools;
}

//get school from schools'id
public school getSchoolById(int id){
    ArrayList<school> allSchools = getAllSchools();
    school target = new school();
    for (school schoolSearch: allSchools){
        if (schoolSearch.getId() == id){
            target = schoolSearch;
        }
    } 
    return target;
}

//get school which total number of student is higher than input param number
public ArrayList<school> getSchoolByInputNoStudent(int inpNumber){
    ArrayList<school> allSchools = new ArrayList<>();
    for (int i=0; i<getAllSchools().size(); i++){
        if (getAllSchools().get(i).getTotalStudent() > inpNumber){
            allSchools.add(getAllSchools().get(i));
        }
    }
    return allSchools;
}




}
