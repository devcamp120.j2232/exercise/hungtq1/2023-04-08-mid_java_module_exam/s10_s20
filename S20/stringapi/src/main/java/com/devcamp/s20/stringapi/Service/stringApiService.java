package com.devcamp.s20.stringapi.Service;

//import java.util.Arrays;

import org.springframework.stereotype.Service;

@Service
public class stringApiService {
    
    //reverse a string
    public String reverseString(String inpString) {
        StringBuilder reverse = new StringBuilder();
        for (int i = inpString.length() - 1; i >= 0; i--) {
            reverse.append(inpString.charAt(i));
        }
        return reverse.toString();
    }

    //validate palindrome string
    public boolean isPalindrome(String inpString) {
        
          
        StringBuilder compareString = new StringBuilder(inpString);
        return inpString.equals(compareString.reverse().toString());
    }

    //remove duplicate character in a string
    public String removeDuplicateCharacter(String inpString) {
        String outputString = new String();
        int inpStringLength = inpString.length();
        int i, j;
    
        for (i=0; i < inpStringLength; i++){
            for (j=0; j < i; j++){
                if (inpString.charAt(i)== inpString.charAt(j)){
                    break;
                }
            }
            if (j==i){
                outputString += inpString.charAt(i);
            }
        }
        return outputString;
    }

    //compare length of two strings then concatenate them after cutting
    public String concatenateStrings(String str1, String str2) {
        
        if (str1.length() > str2.length()) {
            str1 = str1.substring(str1.length() - str2.length());
        } 
        else if (str2.length() > str1.length()) {
            str2 = str2.substring(str2.length() - str1.length());
        }
        return str1 + str2;
    }
}
