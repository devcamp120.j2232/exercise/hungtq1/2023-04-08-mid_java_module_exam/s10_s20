package com.devcamp.s20.stringapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringapiApplication.class, args);
	}

}
