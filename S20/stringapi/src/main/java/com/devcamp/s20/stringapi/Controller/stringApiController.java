package com.devcamp.s20.stringapi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.s20.stringapi.Service.stringApiService;

@RestController
@CrossOrigin
@RequestMapping("/")

public class stringApiController {
    @Autowired
    public stringApiService stringApiService;

    //reverse a string
    @GetMapping("/reverse")
    public String getReverseString(@RequestParam(name = "string", required = true) String inpString){
        return stringApiService.reverseString(inpString);
    }

    //validate palindrome string
    @GetMapping("/palindrome")
    public boolean checkPalindrome(@RequestParam(name = "string", required = true) String inpString){
        return stringApiService.isPalindrome(inpString);
    }

    //remove duplicate character in a string
    @GetMapping("/double-character-remove")
    public String removeDulString(@RequestParam(name = "string", required = true) String inpString){
        return stringApiService.removeDuplicateCharacter(inpString);
    }

    //compare length of two strings then combine them after cutting
    @GetMapping("/compare-cut-concatenate")
    public String cutAndDulString(@RequestParam String string1, String string2){
        return stringApiService.concatenateStrings(string1,string2);
    }

}
